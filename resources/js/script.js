var scores, roundScore, activePlayer, dice, dice1, gamePlaying;



init();

document.querySelector('.btn-new').addEventListener('click', init);

//document.getElementById('name-' + activePlayer).classList.toggle('active-player-name');



document.querySelector('.btn-roll').addEventListener('click', function () {

    if (gamePlaying) {

        //1. Random number
        dice = Math.floor(Math.random() * 6) + 1;
        console.log(dice);
        var diceDOM = document.querySelector('.dice');

        //2. Display
        diceDOM.style.display = 'block';
        diceDOM.src = 'resources/img/dice-' + dice + '.png';

        // 3. Score update
        if (dice > 1) {
            roundScore = roundScore + dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }
    }

});


document.querySelector('.btn-hold').addEventListener('click', function () {

    if (gamePlaying) {
        scores[activePlayer] = scores[activePlayer] + roundScore;
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

        if (scores[activePlayer] >= 50) {
            document.querySelector('#name-' + activePlayer).textContent = "Winner!";
            document.querySelector('#name-' + activePlayer).classList.add('winner-player-name');
            document.querySelector('#name-' + activePlayer).classList.add('shake-slow');
            document.querySelector('#name-' + activePlayer).classList.add('shake-constant');
            gamePlaying = false;

        } else {
            nextPlayer();
        }
    }

});

function nextPlayer() {
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;

    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');

    document.getElementById('name-0').classList.toggle('active-player-name');
    document.getElementById('name-1').classList.toggle('active-player-name');
}

function init() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gamePlaying = true;

    document.querySelector('.dice').style.display = 'none';

    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';

    document.querySelector('#name-0').textContent = "Player 1";
    document.querySelector('#name-1').textContent = "Player 2";

    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');

    document.getElementById('name-0').classList.remove('active-player-name');
    document.getElementById('name-1').classList.remove('active-player-name');
    document.getElementById('name-0').classList.add('active-player-name');

    document.getElementById('name-0').classList.remove('winner-player-name');
    document.getElementById('name-1').classList.remove('winner-player-name');

    document.querySelector('#name-0').classList.remove('shake-slow');
    document.querySelector('#name-1').classList.remove('shake-slow');
    document.querySelector('#name-0').classList.remove('shake-constant');
    document.querySelector('#name-1').classList.remove('shake-constant');

}


//document.querySelector('#current-' + activePlayer).textContent = dice;
