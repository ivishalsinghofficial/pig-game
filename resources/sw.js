/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "css/medias.css",
    "revision": "170a33aae56f0e3adaad471a731e103e"
  },
  {
    "url": "css/style.css",
    "revision": "45fc5ab8db0091bf99128c8687dd9153"
  },
  {
    "url": "img/238137.jpg",
    "revision": "6ac0b8f5f4a6d48cc278d2738c9df6b3"
  },
  {
    "url": "img/dice-1.png",
    "revision": "8604c2902fd0096975f7b019ae14ab37"
  },
  {
    "url": "img/dice-2.png",
    "revision": "0e79c24d2d7180a358c3c692704f1b55"
  },
  {
    "url": "img/dice-3.png",
    "revision": "a4c3308284fcc6fa031de9964cf85349"
  },
  {
    "url": "img/dice-4.png",
    "revision": "705ee68dee0ea00167e8b1a764883595"
  },
  {
    "url": "img/dice-5.png",
    "revision": "afaa3e0e4ffc621246aacd60fabbd6fa"
  },
  {
    "url": "img/dice-6.png",
    "revision": "6fe447492616677c75096a9b9406435d"
  },
  {
    "url": "index.html",
    "revision": "a4d644aff9f609e005d0b9b4e0dca277"
  },
  {
    "url": "js/script.js",
    "revision": "1c058d94383d358f4fd3dd3b2c083ac1"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
