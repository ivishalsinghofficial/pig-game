module.exports = {
  "globDirectory": "resources/",
  "globPatterns": [
    "**/*.{css,jpg,png,html,js}"
  ],
  "swDest": "resources\\sw.js"
};